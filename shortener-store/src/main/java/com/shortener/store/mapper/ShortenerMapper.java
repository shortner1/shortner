package com.shortener.store.mapper;

import com.shortener.store.jpo.ShortenerJpo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShortenerMapper {

    int create(ShortenerJpo jpo);

    ShortenerJpo selectOne(int idx);

}
