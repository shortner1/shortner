package com.shortener.store.jpo;

import com.shortener.domain.entity.Shortener;
import lombok.*;
import org.apache.ibatis.type.Alias;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Alias("shortenerJpo")
public class ShortenerJpo implements Serializable {

	private static final long serialVersionUID = -5318878161053888886L;

	private int idx;

	private String url;

	public ShortenerJpo(Shortener shortener){
		if(shortener != null){
			BeanUtils.copyProperties(shortener, this);
		}
	}

	public Shortener toDomain(){
		Shortener shortener = new Shortener();
		BeanUtils.copyProperties(this, shortener);

		return shortener;
	}

}
