package com.shortener;

import com.shortener.domain.entity.Shortener;
import com.shortener.domain.store.ShortenerStore;
import com.shortener.store.jpo.ShortenerJpo;
import com.shortener.store.mapper.ShortenerMapper;
import org.springframework.stereotype.Component;

@Component
public class ShortenerMapperStore implements ShortenerStore {

    private final ShortenerMapper shortenerMapper;

    public ShortenerMapperStore(ShortenerMapper shortenerMapper){
        this.shortenerMapper = shortenerMapper;
    }

    @Override
    public int create(Shortener shortener) {
        ShortenerJpo jpo = new ShortenerJpo(shortener);
        shortenerMapper.create(jpo);

        return jpo.getIdx();
    }

    @Override
    public Shortener selectOne(int idx) {
        ShortenerJpo jpo = shortenerMapper.selectOne(idx);
        if(jpo == null){
            return null;
        } else {
            return jpo.toDomain();
        }
    }
}