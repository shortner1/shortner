package com.shortener.domain.service;

public interface ShortenerService {
    String addUrl(String url);

    String proxyUrl(String uri);
}