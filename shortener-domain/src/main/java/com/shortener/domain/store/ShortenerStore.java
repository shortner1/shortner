package com.shortener.domain.store;

import com.shortener.domain.entity.Shortener;

public interface ShortenerStore {

    int create(Shortener shortener);

    Shortener selectOne(int idx);

}