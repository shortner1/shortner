package com.shortener.rest;

import com.shortener.domain.entity.Shortener;
import com.shortener.domain.service.ShortenerService;
import com.shortener.util.ProxyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin("*")
@RequestMapping
public class ShortenerResource {

    private final ShortenerService shortenerService;

    public ShortenerResource(ShortenerService shortenerService){
        this.shortenerService = shortenerService;
    }

    @GetMapping("/{uri}")
    public ResponseEntity<Object> proxyUrl(@PathVariable("uri") String uri){

        log.info("Start ProxyUrl ==> " + uri);
        String res = shortenerService.proxyUrl(uri);

        if(res == "404"){
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }

        if(res == "500"){
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<Object>(res, HttpStatus.OK);
    }

    @PostMapping("/shortener")
    public String addUrl(@RequestBody Shortener shortener){
        log.info("Start addUrl ==> " + shortener.getUrl());

        if(shortener.getUrl() == null){
            return "url cannot be null.";
        }

        return shortenerService.addUrl(shortener.getUrl());
    }

}