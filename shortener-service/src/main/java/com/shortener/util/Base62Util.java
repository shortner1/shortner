package com.shortener.util;

import java.util.Base64;

public class Base62Util {
    final static int RADIX = 62;
    final static String CODEC = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static String encoding(int param) {
        StringBuffer sb = new StringBuffer();
        while(param > 0) {
            sb.append(CODEC.charAt(param % RADIX));
            param /= RADIX;
        }
        return sb.toString();
    }

    public static int decoding(String param) {
        int sum = 0;
        int power = 1;
        for (int i = 0; i < param.length(); i++) {
            sum += CODEC.indexOf(param.charAt(i)) * power;
            power *= RADIX;
        }
        return sum;
    }
}
