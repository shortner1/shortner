package com.shortener.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;

import java.io.IOException;

@Slf4j
public class ProxyUtil {

    public static String requestGet(String url){
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(url);
        String response = null;
        try {
            client.executeMethod(method);
            if (method.getStatusCode() == HttpStatus.SC_OK) {
                response = method.getResponseBodyAsString();
            } else {
                log.warn("call [" + url + "] response status ==> " + method.getStatusCode());
            }
        } catch (IOException e) {
            log.error("ProxyUtil.requestGet Error!", e);
        }

        return response;
    }

}
