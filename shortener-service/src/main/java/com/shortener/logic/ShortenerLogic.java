package com.shortener.logic;

import com.shortener.domain.entity.Shortener;
import com.shortener.domain.service.ShortenerService;
import com.shortener.domain.store.ShortenerStore;
import com.shortener.util.Base62Util;
import com.shortener.util.ProxyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ShortenerLogic implements ShortenerService {

    private final ShortenerStore shortenerStore;

    public ShortenerLogic(ShortenerStore shortenerStore){
        this.shortenerStore = shortenerStore;
    }

    @Value("${local.url:http://localhost:8080/}")
    String shortenerUrl;

    @Override
    public String addUrl(String url) {
        Shortener s = new Shortener();
        s.setUrl(url);

        int idx = shortenerStore.create(s);

        return shortenerUrl + Base62Util.encoding(idx);
    }

    @Override
    public String proxyUrl(String uri) {
        int idx = Base62Util.decoding(uri);

        Shortener shortener = shortenerStore.selectOne(idx);

        if(shortener == null){
            return "404";
        }

        String res = ProxyUtil.requestGet(shortener.getUrl());

        if(res == null){
            return "500";
        }

        return res;
    }
}
